'use strict'
// const API_KEY = 'c9006e4a486493a669abf7134244020f' //teste
const API_KEY = '09fecfa8018ec877df2d7bc0326e9846' //producao
const ACCOUNT_ID = '894AC9B8AF724C6392BD52B0AB648EFC'
const iugu = require('./iugu')(API_KEY)
const account_test = require('./accounts')

var testeCriarSubConta = (next)=>{
    var options = { 
            name: 'PerfectBody academia',
            commissions:{
              percent: 10,
              credit_card_percent: 10
            }
    }
    iugu.criarSubConta(options)
      .then((res)=>{
          next(res)
    })
}

var testeVerificarSubConta = next=>{
  var sub_account = {
      account_id: "8A3083BF36DE4C6B8080D4A9D2652957",
      name: "aje_mulher",
      live_api_token: "837ab91e15ce1967689a701e391f4840",
      test_api_token: "fb7bc12122960c8bddda17b67e5c9a1e",
      user_token: "42e9b5beba4b284beb0f3b3186db66ce"
  }
  var options = {
    data: { 
          price_range: 'Mais que R$ 500,00',
          physical_products: false,
          business_type: 'aqui fazemos papel direito',
          person_type: 'Pessoa Física',
          cpf: '94882592215',
          automatic_transfer: false,
          address: 'Tv 3 de Maio n° 830',
          cep: '66063383',
          city: 'Belem',
          state: 'Pará',
          telephone: '9191641767',
          name: 'Maria Celestina',
          bank: 'Banco do Brasil',
          bank_ag: '0765-X',
          account_type: 'Corrente',
          bank_cc: '50726-1'
        }
    }
    iugu.mudarTokenApi(sub_account.user_token)
      .verificarSubConta(sub_account.account_id, options)
      // .then(res=>{
      //     next(res)
      // })
}

var testeConfigurarSubConta = next => {
  var sub_account = account_test[1]
  var options = { 
    auto_withdraw: 'false',
    auto_advance: 'true',
    fines: 'false',
    per_day_interest: 'false',
    auto_advance_type: 'monthly',
    auto_advance_option: 1,
    credit_card: 
    { 
      installments: 'true', 
      active: 'true', 
      max_installments: 3 
    } 
  }
  iugu.mudarTokenApi(sub_account.user_token)
  .configurarSubConta(options)
  .then((res)=>{
    next(res)
  })
}

var testeCriarToken = (next, card_data) => {
    var card_data = {
      account_id: ACCOUNT_ID,
      method: 'credit_card',
      test: true,
      data:{
        number:'4242424242424242',
        verification_value:'1234',
        first_name:'Joao',
        last_name:'Silva',
        month:'03',
        year:'22'
      }
    }
    iugu.novoTokenCartao(card_data)
    .then(res=>{
      next(res)
    })
}

var testeCriarCobranca = (cc_token) => {

  var charge_data = { 
   token: cc_token,
   email: 'leohenryster@gmail.com',
   months: 1,
   items: [ { description: 'Liquidificador', quantity: 2, price_cents: 16100 } ],
   payer: 
    { 
      cpf_cnpj: '01377139247',
      name: 'leonam silva',
      phone_prefix: '91',
      phone: '982502382',
      email: 'leohenryster@gmail.com',
      address: 
       { zip_code: '67125842',
         street: 'SN 3',
         number: '17',
         district: 'Icui gauajara',
         city: 'Ananindeua',
         state: 'PA',
         country: 'Brasil',
         complement: 'Cj uirapuru' 
        } 
    }
  }
  iugu.novaCobranca(charge_data)
  .then(res => {
    console.log(res)
  })
}

var testeTransferirValor = () =>{
    let transferOptions = {
      receiver_id:"8A3083BF36DE4C6B8080D4A9D2652957",
      amount_cents: 10
    }
    iugu.fazerTransferencia(transferOptions)
    .then(res=>{
      console.log(res)
    })
}

// testeCriarSubConta(data=>{console.log(data)})
testeConfigurarSubConta(data=>{console.log(data)})
    





