'use strict'
const request = require('superagent')
const Q = require('q')

const API = {
    'charge': 'https://api.iugu.com/v1/charge',
    'payment_token': 'https://api.iugu.com/v1/payment_token',
    'marketplace_account': 'https://api.iugu.com/v1/marketplace/create_account',
    'configuration':'https://api.iugu.com/v1/accounts/configuration',
    'request_verification': 'https://api.iugu.com/v1/accounts/:account_id/request_verification',
    'tranfers': 'https://api.iugu.com/v1/transfers'
}

const iugu = (token) => {
    let that = this
    that.api_token = token

    that.mudarTokenApi = token => {
        that.api_token = token
        return that
    }
    
    that.novaCobranca = options => {
        options.api_token = that.api_token
        return Q(request.post(API.charge)
            .send(options)
            .then(err, res => {
                return res.body
            })
            .catch(err => {
                console.log(err)
            })
        )}

    that.criarSubConta = options => {
        options.api_token = that.api_token
        return Q(request.post(API.marketplace_account)
            .send(options)
            .set('Accept', 'application/json')
        ).then(res=>{
            return res.body
        })
        .catch(err => {
            console.log(err)
        })
    }

    that.configurarSubConta = options => {
        options.api_token = that.api_token
        return Q(request.post(API.configuration)
            .send(options)
            .set('Accept', 'application/json')
        ).then(res=>{
            return res.body
        })
        .catch(err => {
            console.log(err)
        })
    }

    that.verificarSubConta = (account_id, dados_verificação) => {
        dados_verificação.api_token = that.api_token
        let url = API.request_verification.replace(':account_id', account_id)
        request.post(url)
            .send(dados_verificação)
            .set('Accept', 'application/json')
        .then(res=>{
            return res.body
        })
        .catch(err => {
            console.log(err)
        })
    }

    that.novoTokenCartao = options => {
        options.api_token = that.api_token
        return Q(request.post(API.payment_token)
            .send(options)
            .set('Accept', 'application/json')
            .then(res => {
                return res.body
            })
            .catch(err => {
                console.log(err)
            })
        )}

    that.fazerTransferencia = options => {
        options.api_token = that.api_token
        return Q(request.post(API.tranfers)
            .send(options)
            .set('Accept', 'application/json'))
            .then(res=>{
                return res.body
            })
            .catch(err => {
                console.log(err)
            })
    }

    return that
}

module.exports = iugu